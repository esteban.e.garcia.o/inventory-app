import { Component, OnInit } from '@angular/core';
import { Product } from '../interfaces/product';
import { Config } from '../interfaces/config';
import { Router } from '@angular/router';
import { ApiService } from '../api/api.service';


@Component({
  selector: 'app-stock',
  templateUrl: './stock.page.html',
  styleUrls: ['./stock.page.scss'],
})
export class StockPage implements OnInit {
  products: Product[] = [];
  
  constructor(public config: Config, private router: Router, private api: ApiService) {} 

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.products = this.config.getProducts();
  }

  updateProducts() {
    if (this.config.getTotalClients() > 0 || this.config.getTotalDeliveries() > 0 || this.config.getTotalPayments() > 0) {
      this.config.getMessage('Imposible actualizar productos, tienes clientes o entregas o abonos que enviar, para actualizar productos es necesario enviarlos. Gracias');
      this.router.navigate(['/home']);
    } else {
      this.config.presentLoading();
      const provider = this.config.getProvider();
      this.api.getProducts(provider.id).then((response: any) => {
        if (response.products) {
          this.config.setProducts(response.products);
          this.getProducts();
          this.config.dismissLoader();
        }
      });
    }
  }

}
