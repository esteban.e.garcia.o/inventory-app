import { Component, OnInit } from '@angular/core';
import { Config } from '../interfaces/config';
import { ApiService } from '../api/api.service';
import { Provider } from '../interfaces/provider';
import { Delivery } from '../interfaces/delivery';
import { Payment } from '../interfaces/payment';
import { Client } from '../interfaces/client';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  public totalClients = 0;
  public totalDeliveries = 0;
  public totalPayments = 0;
  constructor(
    private config: Config,
    private apiService: ApiService,
    public alertController: AlertController
  ) { }

  ngOnInit() {
    if (!this.config.checkToday()) {
      if (this.config.getTotalClients() > 0 || this.config.getTotalDeliveries() > 0 || this.config.getTotalPayments() > 0) {
        this.config.getMessage('Imposible cargar los datos del día aún tienes clientes o entregas o abonos pendientes por enviar, para continuar debes enviarlos. Gracias');
      } else
        this.getDailyInformation();
    }
    this.getTotals();
  }

  getTotals() {
    this.totalClients = this.config.getTotalClients();
    this.totalDeliveries = this.config.getTotalDeliveries();
    this.totalPayments = this.config.getTotalPayments();
  }

  getDailyInformation() {
    this.config.presentLoading();
    let provider: Provider = this.config.getProvider();
    this.apiService.dailyInformation({id: provider.id}).then((response: any) => {
      if (response.error)
        this.config.getMessage(response.message);
      else {        
        this.config.dismissLoader();
        this.config.setClients(response.clients);
        this.config.setProducts(response.products);
        this.config.setDeliveries(response.deliveries);
        window.location.reload();
      }
    });
  }

  sendInformation(type) {
    if (navigator.onLine) {
      this.config.presentLoading();
      let index = 0;
      switch (type) {
        case "clients":          
          let clients: Client[] = this.config.getClients().filter(client => client.is_new == true);
          this.sendClients(index, clients);                   
          break;
        case "deliveries":          
          let deliveries: Delivery[] = this.config.getDeliveries().filter(delivery => delivery.is_new == true);
          this.sendDeliveries(index, deliveries);          
          break;
        case "payments":          
          let payments: Payment[] = this.config.getPayments().filter(payment => payment.is_new == true);
          this.sendPayments(index, payments);          
          break;
        default:
          break;
      }
    } else 
      this.config.getMessage('Lo sentimos tu dispositivo no cuenta con una conexión a internet');   
  }

  sendDeliveries(index: number, deliveries: Delivery[]) {    
    const delivery = deliveries[index];
    this.apiService.sendDelivery(delivery).then((response: any) => {
      if (response.error) {
        this.config.getMessage(response.message);
        this.config.dismissLoader();
      } else {
        delivery.is_new = undefined;
        this.config.editDelivery(delivery);
        index++;
        if (index < deliveries.length)
          this.sendDeliveries(index, deliveries);
        else {
          this.getTotals();
          this.config.dismissLoader();
          this.config.getMessage('Entregas enviadas con éxito', 'Éxito');
        }          
      }
    });
  }

  sendPayments(index: number, payments: Payment[]) {    
    const payment = payments[index];
    this.apiService.sendPayment(payment).then((response: any) => {
      if (response.error) {
        this.config.getMessage(response.message);
        this.config.dismissLoader();
      } else {
        payment.is_new = undefined;
        this.config.editPayment(payment);
        index++;
        if (index < payments.length)
          this.sendPayments(index, payments);
        else {
          this.getTotals();
          this.config.dismissLoader();
          this.config.getMessage('Abonos enviados con éxito', 'Éxito');
        }          
      }
    });
  }

  sendClients(index: number, clients: Client []) {       
    const client = clients[index];
    this.apiService.sendClient(client).then((response: any) => {
      if (response.error) {
        this.config.getMessage(response.message);
        this.config.dismissLoader();
      } else {
        client.is_new = undefined;
        this.config.editClient(client);
        index++;
        if (index < clients.length)
          this.sendClients(index, clients);
        else {
          this.getTotals();
          this.config.dismissLoader();
          this.config.getMessage('Clientes enviados con éxito', 'Éxito');
        }          
      }
    });
  }


  sendClientsLocation(index: number, clients: Client []) {   
    const client = clients[index];
    this.apiService.sendClientLocation(client).then((response: any) => {
      if (response.error) {
        this.config.getMessage(response.message);
        this.config.dismissLoader();
      } else {
        client.is_new = undefined;
        this.config.editClient(client);
        index++;
        if (index < clients.length)
          this.sendClientsLocation(index, clients);
        else {
          this.getTotals();
          this.config.dismissLoader();
          this.config.getMessage('Clientes enviados o modificados con éxito', 'Éxito');
        }          
      }
    });
  }

  async sendInformationType(type) {
    let tipo = "";
    switch (type) {
      case "clients":
        tipo = "Clientes";
        break;
      case "deliveries":
        if (this.config.getTotalClients() == 0) {
          tipo = "Entregas";
        } else {
          this.config.getMessage("Para enviar las entregas es necesario primero enviar los clientes");
        }
        break;
      case "payments":
        if (this.config.getTotalDeliveries() == 0) {
          tipo = "Abonos";
        } else {
          this.config.getMessage("Para enviar los abonos es necesario primero enviar las entregas");
        }
        break;
      default:
        break;
    }
    if (tipo != "") {
      const alert = await this.alertController.create({
        header: `¿Desea enviar información de ${tipo}?`,
        message: 'confirme por favor',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
            }
          }, {
            text: 'Aceptar',
            handler: () => {
                this.sendInformation(type);
            }
          }
        ]
      });
  
      await alert.present();
    }
  }

}
