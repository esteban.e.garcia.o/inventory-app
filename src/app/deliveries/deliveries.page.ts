import { Component, OnInit } from '@angular/core';
import { Config } from '../interfaces/config';
import { Client } from '../interfaces/client';
import { Product } from '../interfaces/product';
import { Delivery } from '../interfaces/delivery';
import { Provider } from '../interfaces/provider';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deliveries',
  templateUrl: './deliveries.page.html',
  styleUrls: ['./deliveries.page.scss'],
})
export class DeliveriesPage implements OnInit {

  public clients: Client[] = [];
  public products: Product[] = [];
  public product_id: string;
  public delivery: Delivery = {
    id: '',
    client_id: '',
    user_id: '',
    initial_payment: 0,
    type: 'N',
    total: 0,
    products: [],
    payments: [],
    is_new: true
  };
  public quantity = 0;
  public total = 0;
  public isCredit = false;
  public provider: Provider;
  public client: Client;
  constructor(public config: Config, private route: Router) { }

  ngOnInit() {
    this.provider = this.config.getProvider();
    this.clients = this.config.getClients();
    this.products = this.config.getProducts().filter(product => product.pivot.quantity > 0);
  }

  addProduct() {
    if (this.product_id) {
      const productExist = this.delivery.products.find((product) => product.id == this.product_id);
      if (!productExist) {
        const product = this.products.find((product) => product.id == this.product_id);
        if (product) {
          this.delivery.products.push({
            id: product.id,
            name: product.name,
            price: product.price,
            quantity: product.quantity,
            pivot: product.pivot
          });
        }        
      } else 
        this.config.getMessage('El producto seleccionado ya fue agregado');
    } else {
      this.config.getMessage('Favor de seleccionar un producto');
    }    
  }

  checkStock(event, product) {    
    const element = event.currentTarget;
    let quantity = element.value;
    if (quantity !== "") {
      quantity = parseInt(quantity);
      if (quantity <= product.pivot.quantity) {
        product.quantity = quantity;
        this.updateTotal();
      } else {
        element.value = '0';
        this.config.getMessage('La cantidad ingresada rebasa la existencia del producto, solo quedan: ' + product.pivot.quantity);
      }
    } else 
      this.config.getMessage('Favor ingrese la cantidad');    
  }

  save() {    
    if (this.delivery.products.length > 0) {
      const checkProducts = this.delivery.products.filter(product => product.quantity == undefined);
      if (checkProducts.length == 0) {
        if (this.delivery.total > 0) {
          this.delivery.products.map(product => {               
            let productProvider = this.products.find(pro => pro.id == product.id);
            let quantity = productProvider.pivot.quantity;
            productProvider.pivot.quantity = (parseInt(quantity) - product.quantity);
          });
          this.delivery.id = this.config.getUuid();
          this.delivery.type = (this.isCredit ? 'C' : 'N');
          this.delivery.user_id = this.provider.id;
          this.delivery.client_id = this.client.id;
          this.config.addDelivery(this.delivery);
          this.config.setProducts(this.products);
          this.config.getMessage('Entrega creada', 'Éxito');  
          this.route.navigate(['/home']);
          this.delivery = {
            id: '',
            client_id: '',
            user_id: '',
            initial_payment: 0,
            type: 'N',
            total: 0,
            products: [],
            payments: [],
            is_new: true
          };
          this.isCredit = false;
          this.client = null;
        } else {
          this.config.getMessage('Favor de ingresar el total de la entrega');  
        }
      } else {
        this.config.getMessage('Favor de ingresar la cantidad en los productos');
      }
    } else {
      this.config.getMessage('Favor de seleccionar al menos un producto');
    }    
  }

  updateTotal() {
    this.total = 0;
    this.delivery.products.map(product => this.total += (product.price * product.quantity));
  }

  delete(product) {
    this.delivery.products = this.delivery.products.filter(pro => pro.id !== product.id);
    this.updateTotal();
  }

  selectClient(client: Client) {
    this.client = client;
  }

  search(event) {
    const query = event.target.value.toLowerCase();
    if (query == "") 
      this.clients = this.config.getClients();
    else {
      requestAnimationFrame(() => {
        this.clients = this.clients.filter(client => client.longname.toLowerCase().indexOf(query) > -1);
      });
    }    
    console.log(query);
  }
}
