import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Config } from './interfaces/config';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Inicio',
      url: '/home/',
      icon: 'home'
    },
    {
      title: 'Clientes',
      url: '/clientes/list',
      icon: 'person'
    },
    {
      title: 'Productos en existencia',
      url: '/stock/',
      icon: 'list-circle'
    },    
    {
      title: 'Crear entrega',
      url: '/deliveries/create/',
      icon: 'gift'
    },
    {
      title: 'Entregas',
      url: '/deliveries-list/',
      icon: 'list-circle'
    }, 
    {
      title: 'Abonos',
      url: '/pendients/',
      icon: 'heart'
    },
    {
      title: 'Cerrar sesión',
      url: '/logout/yes',
      icon: 'log-out'
    }

  ];
  public longname = '';
  public version = '';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public config: Config
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }

    const provider = this.config.getProvider();
    if (provider)
      this.longname = provider.longname;

    this.version = this.config.getVersion();
  }
}
