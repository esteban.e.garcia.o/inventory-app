import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Client } from '../interfaces/client';
import { Config } from '../interfaces/config';
import { NgForm } from '@angular/forms';
import { ApiService } from '../api/api.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit {
  client:Client={
    id:'',
    longname:"",
    direction:"",
    phone:"",
    status:true,
    city:"",
    name_store:"",
    location:{},
    is_new:true
  };

  lng:any;
  lat:any;
  checkedLocation:any;
  
  
  
  constructor(private router: Router, public config: Config, public api: ApiService) {  
  }

  ngOnInit() {
    
  }

  delivers() {
    this.router.navigate(['/delivers/']);
  }

  clientForm(fRegister:NgForm){
    if(fRegister.invalid){ return this.config.getMessage("Hace falta datos");}
    if(this.lat != null && this.lng != null){
      this.client.id= this.config.getUuid();
      
      this.client.location={
          "lng": this.lng,
          "lat": this.lat
        };
      if(this.client){
        this.config.addClient(this.client);
        this.config.getMessage("Guardado con exito","Exito");
        this.client={
          id:'',
          longname:"",
          direction:"",
          phone:"",
          name_store:"",
          status:true,
          location:{},
          is_new:true
        };
        
        this.router.navigateByUrl('home');
        
      }else{
        return this.config.getMessage("No se inserto cliente");
      }
    }else{
      return this.config.getMessage("Obtenga Ubicación por favor");   
    }
  }


  getLocation(){
    if (navigator.geolocation){
      this.getPosition().then(pos=>
        {
          this.lng = pos.lng;
          this.lat = pos.lat;
          this.checkedLocation = document.getElementById('location');
          if(this.lng != null && this.lat !=null){
            this.checkedLocation.checked=true; 
            this.config.getMessage("Ubicación obtenida","Exito");
          } 
        });
    }
  }

 getPosition(): Promise<any>{
   return new Promise((resolve, reject) => {

     navigator.geolocation.getCurrentPosition(resp => {

         resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
       },
       err => {
         reject(err);
       });
   });

 }
 
}
