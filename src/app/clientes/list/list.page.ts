import { Component, OnInit, Provider } from '@angular/core';
import { Client } from 'src/app/interfaces/client';
import { Config } from '../../interfaces/config';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiService } from '../../api/api.service';





@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit{
  
listClient:any;
client:Client={
  id:'',
  longname:"",
  direction:"",
  phone:"",
  name_store:"",
  location:{},
  is_new:true
};
lng:any;
lat:any;



  constructor(private apiService: ApiService,
    private router: Router, public config: Config,public alertController: AlertController) {
    this.listClient = this.config.getClients();
  }

  ngOnInit() {
    this.listClient = this.config.getClients(); 
  }

  


  addClient(){
    this.router.navigate(['/clientes/']);
  }

  

  getLocation(client:Client){
    if (navigator.geolocation){
      this.getPosition().then(pos=>
        {          
          this.lng = pos.lng;
          this.lat = pos.lat;
          
          if(this.lng != null && this.lat !=null){
            this.config.getMessage("Ubicación obtenida","Exito");
            client.location={
              "lng":this.lng,
              "lat":this.lat
            }
            this.config.editClient(client);
            this.apiService.sendClientLocation(client);
            this.router.navigateByUrl('clientes/list');
          } 
        });
    }
  }

 getPosition(): Promise<any>
 {
   return new Promise((resolve, reject) => {

     navigator.geolocation.getCurrentPosition(resp => {

         resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
       },
       err => {
         reject(err);
       });
   });

 }


 async presentAlertConfirm(client:Client) {

    const alert = await this.alertController.create({
      header: `¿Desea obtener ubicación de ${client.name_store}?`,
      message: 'confirme por favor',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            this.getLocation(client);
          }
        }
      ]
    });

    await alert.present();
 
  }

  updateClients() { 
    this.listClient = this.config.getClients();
  }

  loadLocation(client:Client){    
    let lng = parseFloat(client.location["lng"]);
    let lat = parseFloat(client.location["lat"]);
    window.open(
      `https://maps.google.com/maps?q=loc:${lat},${lng}`,
      '_blank'
    ); 
    
  }


}
