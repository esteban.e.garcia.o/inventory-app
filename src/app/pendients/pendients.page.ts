import { Component, OnInit } from '@angular/core';
import { Config } from '../interfaces/config';
import { Client } from '../interfaces/client';
import { Payment } from '../interfaces/payment';
import { Delivery } from '../interfaces/delivery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pendients',
  templateUrl: './pendients.page.html',
  styleUrls: ['./pendients.page.scss'],
})
export class PendientsPage implements OnInit {

  public clients: Client[] = [];
  public client_id: string;
  public title = "Abonos";
  public payment: Payment = {
    id: '',
    client_id: '',
    delivery_id: '',
    user_id: '',
    payment: 0,
    is_new: true,
    created: new Date().getDate() + "/" + new Date().getMonth() + "/" + new Date().getFullYear()
  }
  public deliveries: Delivery[] = [];
  public isCreate = false;
  public payments: Payment[] = [];
  constructor(public config: Config, private router: Router) { }

  ngOnInit() {
    this.clients = this.config.getClients();
  }

  selectClient() {
    if (this.payment.client_id !== "") {
      this.deliveries = this.config.getDeliveries().filter(delivery => delivery.type == "C" && delivery.client_id == this.payment.client_id);      
    } else 
      this.config.getMessage('Favor de seleccionar un cliente');
  }

  goCreate(delivery) {
    this.title = "Para esta entrega restan: " + this.config.formatPrice((parseInt(delivery.total) - parseInt((delivery.payment ? delivery.payment : delivery.initial_payment))));
    this.isCreate = true;
    this.payments = delivery.payments;
    this.payment.delivery_id = delivery.id;
    this.payment.payment = 0;
  }

  back() {
    this.title = "Abonos";
    this.isCreate = false;
    this.payments = [];    
    this.payment.id = '';
  }

  save() {
    if (this.payment.payment > 0) {
      let provider = this.config.getProvider();
      this.payment.id = this.config.getUuid();
      this.payment.user_id = provider.id;      
      this.config.addPayment(this.payment);
      this.config.getMessage('Abono agregado', 'Éxito');      
      this.deliveries.map(delivery => {
        if (delivery.id == this.payment.delivery_id) {
          delivery.payment = ((delivery.payment ? delivery.payment : delivery.initial_payment) + this.payment.payment);
          delivery.payments.push(this.payment);
          this.config.editDelivery(delivery);
        }
      });      
      this.back();     
      this.selectClient();
    } else 
      this.config.getMessage('Favor de ingresar un abono');    
  }
}
