import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PendientsPage } from './pendients.page';

describe('PendientsPage', () => {
  let component: PendientsPage;
  let fixture: ComponentFixture<PendientsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendientsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PendientsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
