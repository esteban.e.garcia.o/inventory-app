import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PendientsPage } from './pendients.page';

const routes: Routes = [
  {
    path: '',
    component: PendientsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PendientsPageRoutingModule {}
