import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PendientsPageRoutingModule } from './pendients-routing.module';

import { PendientsPage } from './pendients.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PendientsPageRoutingModule
  ],
  declarations: [PendientsPage]
})
export class PendientsPageModule {}
