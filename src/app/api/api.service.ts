import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from '../interfaces/product';
import { Client } from '../interfaces/client';
import { Config } from '../interfaces/config';
import { Provider } from '../interfaces/provider';
import { Delivery } from '../interfaces/delivery';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  //API_ENDPOINT="http://salty-wildwood-29890.herokuapp.com/api/";  
  API_ENDPOINT = "https://copalist.net/api/";  
  constructor(public _httpClient:HttpClient, private config: Config) { }
 
  getProducts(id){
    let provider: Provider = this.config.getProvider();
    let headers = new HttpHeaders().set("Authorization", `Bearer ${provider.api_token}`);
    return new Promise((resolve,reject)=>{
      this._httpClient.post(this.API_ENDPOINT + 'provider/products', {id}, {headers})
      .subscribe((res: Product[]) => {
          resolve(res);
      }, (error) => {
        reject(error);
      });
    });
  }

  checkLogin(data) {
    let headers = new HttpHeaders().set("Access-Control-Allow-Origin", window.location.origin);
    return new Promise((resolve, reject) => {
      this._httpClient.post(this.API_ENDPOINT + 'provider/checkLogin', data, {headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  dailyInformation(data) {
    let provider: Provider = this.config.getProvider();
    let headers = new HttpHeaders().set("Authorization", `Bearer ${provider.api_token}`);
    return new Promise((resolve, reject) => {
      this._httpClient.post(this.API_ENDPOINT + 'provider/daily/information', data, {headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  sendDelivery(delivery: Delivery) {
    let provider: Provider = this.config.getProvider();
    let headers = new HttpHeaders().set("Authorization", `Bearer ${provider.api_token}`);
    return new Promise((resolve, reject) => {
      this._httpClient.post(this.API_ENDPOINT + 'deliveries', {delivery}, {headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  sendPayment(data) {
    let provider: Provider = this.config.getProvider();
    let headers = new HttpHeaders().set("Authorization", `Bearer ${provider.api_token}`);
    return new Promise((resolve, reject) => {
      this._httpClient.post(this.API_ENDPOINT + 'payments', data, {headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  sendClient(data) {
    let provider: Provider = this.config.getProvider();
    let headers = new HttpHeaders().set("Authorization", `Bearer ${provider.api_token}`);
    return new Promise((resolve, reject) => {
      this._httpClient.post(this.API_ENDPOINT + 'clients', data, {headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  sendClientLocation(data) {
    let provider: Provider = this.config.getProvider();
    let headers = new HttpHeaders().set("Authorization", `Bearer ${provider.api_token}`);
    return new Promise((resolve, reject) => {
      this._httpClient.patch(this.API_ENDPOINT + `clients/${data.id}`, data, {headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  getDeliveries(data) {
    let provider: Provider = this.config.getProvider();
    let headers = new HttpHeaders().set("Authorization", `Bearer ${provider.api_token}`);
    return new Promise((resolve, reject) => {
      this._httpClient.post(this.API_ENDPOINT + 'provider/deliveries', data, {headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }  
  
}
