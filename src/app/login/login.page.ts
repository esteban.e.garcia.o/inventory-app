import { Component, OnInit } from '@angular/core';
import { Provider } from '../interfaces/provider';
import { Config } from '../interfaces/config';
import { ApiService } from '../api/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  provider: Provider = {
    id: '',
    longname: '',
    password: '',
    user_name: '',
    api_token: ''
  }
  public version = '';
  constructor(public apiService: ApiService, public config: Config, private router: Router, private activatedRoute: ActivatedRoute) { 
    if (this.config.check()) {      
      const action = this.activatedRoute.snapshot.paramMap.get('action');
      if (action == "yes") {
        if (config.getTotalClients() > 0 || config.getTotalDeliveries() > 0 || config.getTotalPayments() > 0) {
          config.getMessage('Imposible cerrar sesión, tienes clientes o entregas o abonos que enviar, para cerrar sesión es necesario enviarlos. Gracias');
          this.router.navigate(['/home']);
        } else {
          config.clearAllStorage();
          this.router.navigate(['/']);
        }        
      } else
        this.router.navigate(['/home']);
    } else {
      document.getElementById('ion-menu').style.display = "none";
    }
  }

  ngOnInit() {
    this.version = this.config.getVersion();
  }

  getLogin() {
    this.config.presentLoading();
    this.apiService.checkLogin({user_name: this.provider.user_name, password: this.provider.password}).then((response: any) => {
      this.config.dismissLoader();
      if (response.error)
        this.config.getMessage(response.message);
      else {
        document.getElementById('ion-menu').style.display = "block";
        this.config.setProvider(response.provider);
        this.router.navigate(['/home']);
      }
    });    
  }

}
