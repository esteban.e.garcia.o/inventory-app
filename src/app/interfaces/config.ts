import { Injectable } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { Client } from './client';
import { Delivery } from './delivery';
import { Product } from './product';
import { v4 as uuidv4 } from 'uuid';
import { Payment } from './payment';
@Injectable({
    providedIn: 'root'
})
export class Config {
    public loading = null;
    constructor(public loadingController: LoadingController, public alertController: AlertController){}
    check() {
        return this.getProvider() != null;
    }

    setProvider(provider: any) {
        localStorage.setItem('provider', (provider ? JSON.stringify(provider) : provider));
    }

    getProvider() {
        const provider = localStorage.getItem('provider');
        return (provider ? JSON.parse(provider) : null);
    }

    async presentLoading() {
        this.loading = await this.loadingController.create({
          message: 'Espere por favor...',
          duration: 0
        });        
        await this.loading.present();
    }

    async getMessage(message: string, header = 'Error') {
        const alert = await this.alertController.create({
          header: header,
          subHeader: message,
          message: '',
          buttons: ['OK']
        });
    
        await alert.present();
    }

    addClient(client: Client) {
        let clients: Client[] = this.getClients();
        if (!clients)
            clients = [];
        clients.push(client);
        this.setClients(clients);
    }

    editClient(client:Client) {
        let clients = this.getClients();
        let index = clients.findIndex(cli => cli.id == client.id);
        if (index != -1) {
            clients[index] = client;
            this.setClients(clients);
        }
    }

    setClients(clients: Client[]) {
        localStorage.setItem('clients', JSON.stringify(clients));
    }

    getClients(): Client[] {
        return JSON.parse(localStorage.getItem('clients'));
    }

    addDelivery(delivery: Delivery) {
        let deliveries: Delivery[] = this.getDeliveries();
        if (!deliveries)
            deliveries = [];
        deliveries.push(delivery);
        this.setDeliveries(deliveries);
    }

    setDeliveries(deliveries: Delivery[]) {
        localStorage.setItem('deliveries', JSON.stringify(deliveries));
    }

    getDeliveries(): Delivery[] {
        return JSON.parse(localStorage.getItem('deliveries'));
    }

    editDelivery(delivery: Delivery) {
        let deliveries = this.getDeliveries();
        let index = deliveries.findIndex(deli => deli.id == delivery.id);
        if (index != -1) {
            deliveries[index] = delivery;
            this.setDeliveries(deliveries);
        }
    }

    setProducts(products: Product[]) {
        localStorage.setItem('products', JSON.stringify(products));
    }

    getProducts(): Product[] {
        return JSON.parse(localStorage.getItem('products'));
    }

    addPayment(payment: Payment) {
        let payments: Payment[] = this.getPayments();
        if (!payments)
            payments = [];
        payments.push(payment);
        this.setPayments(payments);
    }

    setPayments(payments: Payment[]) {
        localStorage.setItem('payments', JSON.stringify(payments));
    }

    getPayments(): Payment[] {
        return JSON.parse(localStorage.getItem('payments'));
    }

    editPayment(payment: Payment) {
        let payments = this.getPayments();
        let index = payments.findIndex(pay => pay.id == payment.id);
        if (index != -1) {
            payments[index] = payment;
            this.setPayments(payments);
        }
    }

    clearAllStorage() {localStorage.clear();}

    checkToday() {
        let today = localStorage.getItem('today');
        const date = new Date();
        let todayGenerate = date.getDay() + '-' + date.getMonth() + '-' + date.getFullYear();
        if (today) {            
            if (today == todayGenerate)
                return true;
            else {
                localStorage.setItem('today', todayGenerate);
                return false;
            }
        } else {
            localStorage.setItem('today', todayGenerate);
            return false;
        }
    }

    formatPrice(price) {
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 0
          });
          return formatter.format(price);
    }

    getUuid() {
        return uuidv4();
    }

    getTotalClients() {
        if (!this.getClients())
            return 0;
        return this.getClients().filter(client => client.is_new == true).length;
    }

    getTotalDeliveries() {
        if (!this.getDeliveries())
            return 0;
        return this.getDeliveries().filter(delivery => delivery.is_new == true).length;
    }

    getTotalPayments() {
        if (!this.getPayments())
            return 0;
        return this.getPayments().filter(payment => payment.is_new == true).length;
    }

    getVersion() {
        return '0.0.1';
    }

    async dismissLoader() {
        await this.loading.dismiss()
        .then(()=>{
          this.loading = null;
        })
        .catch(e => console.log(e));
    }

    getDate(format = '-') {
        if (format == '-') {
            return new Date().getFullYear() + '-' + ((new Date().getMonth() + 1) <= 9 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1)) + '-' + (new Date().getDate() <= 9 ? '0' + new Date().getDate() : new Date().getDate())
        } else 
            return (new Date().getDate() <= 9 ? '0' + new Date().getDate() : new Date().getDate()) + '/' + ((new Date().getMonth() + 1) <= 9 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1)) + '/' + new Date().getFullYear()
    }
}
  