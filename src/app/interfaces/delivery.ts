import { Payment } from './payment';
import { Product } from './product';
import { Client } from './client';

export interface Delivery{
    id: string,
    user_id: string,
    client_id: string,
    type: string,
    initial_payment: number,
    total: number,
    products: Product[],
    payments: Payment[],
    is_new: boolean,
    payment?: number,
    client?: Client,
    created?: string
  }