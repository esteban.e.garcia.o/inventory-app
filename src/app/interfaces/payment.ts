export interface Payment{
    id: string,
    user_id: string,
    client_id: string,
    delivery_id: string,
    payment: number,
    is_new?: boolean,
    created?: string
  }