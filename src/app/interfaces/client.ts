export interface Client {
  id:string,
  longname:string,
  direction:string,
  phone:string,
  name_store:string,
  status?:boolean,
  city?:string,
  location?:{},
  created_at?:string,
  updated_at? :string,
  is_new?:boolean
}
