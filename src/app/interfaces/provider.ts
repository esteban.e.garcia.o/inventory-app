export interface Provider {
    id: string,
    user_name: string,
    password: string,
    longname: string,
    api_token: string
  }
  