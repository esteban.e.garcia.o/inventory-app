import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeliveriesListPage } from './deliveries-list.page';

const routes: Routes = [
  {
    path: '',
    component: DeliveriesListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeliveriesListPageRoutingModule {}
