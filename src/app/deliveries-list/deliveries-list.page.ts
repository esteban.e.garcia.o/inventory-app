import { Component, OnInit } from '@angular/core';
import { Config } from '../interfaces/config';
import { Delivery } from '../interfaces/delivery';
import { ApiService } from '../api/api.service';

@Component({
  selector: 'app-deliveries-list',
  templateUrl: './deliveries-list.page.html',
  styleUrls: ['./deliveries-list.page.scss'],
})
export class DeliveriesListPage implements OnInit {

  public deliveries: Delivery[] = [];
  public date: string;

  constructor(public config: Config, private api: ApiService) {
    this.date = config.getDate();    
    console.log(this.date);
  }

  ngOnInit() {
    this.update();
  }

  update() {
    this.deliveries = this.config.getDeliveries().map(delivery => {
      const client = this.config.getClients().find(client => client.id == delivery.client_id);
      delivery.client = client;
      if (!delivery.is_new)
        delivery.is_new = false;
      return delivery;
    });
    this.deliveries = this.deliveries.filter(delivery => !delivery.created);
    this.date = this.config.getDate();
  }

  getDeliveries(data) {
    this.api.getDeliveries(data).then((response: any) => {
      this.config.dismissLoader();
      if (response.deliveries)
        this.deliveries = response.deliveries;
    }).catch((response: any) => {      

    });
  }

  searchDeliveries() {
    if (this.config.getDate() == this.date) {
      this.update();
    }else if (navigator.onLine) {
      this.config.presentLoading();
      let provider = this.config.getProvider();
      this.getDeliveries({id: provider.id, date: this.date});
    } else 
      this.config.getMessage('Lo sentimos tu dispositivo no cuenta con una conexión a internet');   
  }
}
