import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeliveriesListPageRoutingModule } from './deliveries-list-routing.module';

import { DeliveriesListPage } from './deliveries-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeliveriesListPageRoutingModule
  ],
  declarations: [DeliveriesListPage]
})
export class DeliveriesListPageModule {}
